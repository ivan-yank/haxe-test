# download the latest build in an elevated PowerShell console, run the following:
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest https://github.com/PowerShell/Win32-OpenSSH/releases/download/v1.0.0.0/OpenSSH-Win64.zip -OutFile openssh.zip

# extract the zip file in an elevated Powershell console
Expand-Archive .\openssh.zip 'C:\Program Files'

# Launch PowerShell as an Administrator and go to the directory where the files have been extracted
cd 'C:\Program Files\OpenSSH-Win64'

# In an elevated PowerShell console, run
powershell.exe -ExecutionPolicy Bypass -File install-sshd.ps1

# Open the firewall for sshd.exe to allow inbound SSH connections
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
netsh advfirewall firewall add rule name=sshd dir=in action=allow protocol=TCP localport=sshd

# Start sshd
Start-Service sshd

# Prior versions required SSHD resources (sshd_config, host keys and authorized_keys) to have READ access to “NT ServiceSSHD”. This is no longer a requirement and the corresponding ACL entry should be removed.
powershell.exe -ExecutionPolicy Bypass -Command '. .\FixHostFilePermissions.ps1 -Confirm:$false'

# Change the services sshd and ssh-agent to auto-start
Set-Service sshd -StartupType Automatic
Set-Service ssh-agent -StartupType Automatic

# Install Cygwin
#Invoke-WebRequest https://cygwin.com/setup-x86_64.exe -OutFile setup-x86_64.exe

#powershell.exe -ExecutionPolicy Bypass -Command ". "".\setup-x86_64.exe --site http://cygwin.mirror.constant.com --no-shortcuts --no-desktop --quiet-mode --root 'C:\Program Files\cygwinx86_64\cygwin' --arch x86_64 --local-package-dir 'C:\Program Files\cygwinx86_64\cygwin-packages' --verbose --prune-install --packages openssh,git,rsync,nano"""

#Install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install dependencies
choco install visualstudio2017community -y
choco install visualstudio2017buildtools -y
choco install visualstudio2017-workload-vctools -y
choco install visualstudio2017-workload-netcoretools -y
choco install jre8 -y
choco install jdk8 -y
choco install nodejs -y
choco install python -y --version 3.4.2
choco install php -y --version 7.1.26
choco install haxe -y
choco install cygwin -y

# Add php extension
cat C:\tools\php71\php.ini | %{$_-replace ";extension=php_mbstring.dll","extension=php_mbstring.dll"} > C:\tools\php71\php.ini.tmp
mv C:\tools\php71\php.ini C:\tools\php71\php.ini.backup
mv C:\tools\php71\php.ini.tmp C:\tools\php71\php.ini

# Install haxelibs
#haxelib setup C:\Users\vagrant\haxelib
#haxelib install hxcpp
#haxelib install hxjava
#haxelib install hxcs
#haxelib install utest

# Install Haxe
#choco install haxe -y
#Invoke-WebRequest -Uri https://github.com/HaxeFoundation/haxe/releases/download/4.0.0-rc.1/haxe-4.0.0-rc.1-win64.exe -UseBasicParsing -OutFile C:\Users\vagrant\Downloads\haxe4.exe
#Invoke-WebRequest -Uri https://github.com/HaxeFoundation/haxe/releases/download/3.4.7/haxe-3.4.7-win64.exe -UseBasicParsing -OutFile C:\Users\vagrant\Downloads\haxe3.exe
#Invoke-WebRequest -Uri https://github.com/HaxeFoundation/haxe/releases/download/4.0.0-rc.1/haxe-4.0.0-rc.1-win64.exe -UseBasicParsing -OutFile haxe.exe

#powershell.exe -ExecutionPolicy Bypass -Command '. .\haxe.exe'
#https://github.com/HaxeFoundation/haxe/releases/download/4.0.0-rc.1/haxe-4.0.0-rc.1-win64.exe
