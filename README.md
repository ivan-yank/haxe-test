# Haxe for Docker

## Requirements

* Linux 3.10+ or OS X/Windows with <http://boot2docker.io>
* Docker 1.3+
* Python with Fabric3 installed

## Install

```
git clone https://ivan-yank@bitbucket.org/ivan-yank/haxe-test.git
pip install Fabric3
```

## Building image

Linux:
```
fab build_image:$target1,$target2
example "fab build_image:js,php,neko"
```
Windows:
To run in windows it's require vagrant (https://www.vagrantup.com/) instead of docker. 
When vagrant installed run 
```
vagrant up
```
After vagrant would ready restart vm and run last configurations
```
vagrant reload
fab win_configure
```
There we go! You are ready to run in windows.

## Make target

Linux:
```
fab make_target:$target1,$target2
example "fab make_target:js,php,neko"
```
Windows:
```
fab win_make_target:$target1,$target2
example "fab make_target:js,php,neko"
```

## Run tests

Linux:
```
fab test_target:$target1,$target2
example "fab test_target:js,php,neko"

```
Windows:
```
fab win_test_target:$target1,$target2
example "fab test_target:js,php,neko"

```

You should see the following once the build finishes:
```
Test.hx:15: Hello world!
utest/ui/text/PrintReport.hx:52: 
assertations: 2
successes: 2
errors: 0
failures: 0
warnings: 0
execution time: 0

results: ALL TESTS OK (success: true)
HelloWorldTestCase
  testHelloWorld: OK .
SimpleTestCase
  testBasic: OK .

> Hashlink compiled passed

```


## Run test game (tested on linux only)

First build image with target 'testgame'. Next run:
```
fab run_testgame
```
