#!/usr/bin/env bash

# gcc -O3 -o Test -std=c11 -I build/hashlink-c build/hashlink-c/test.c -Lhl -lm
cd build/hashlink-c && gcc -o test test.c -I. -lhl -lSDL2 -lm -lopenal -lGL
./test && echo '> Hashlink compiled passed'
