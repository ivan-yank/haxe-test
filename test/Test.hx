package;
import utest.Test;
import utest.Runner;
import utest.ui.Report;
import utest.Assert;


class HelloWorldTestCase extends Test{

    public function new(){
        super();
    }

    public function testHelloWorld():Void {
        trace("Hello world!");
        Assert.isTrue(true);
    }
}


class SimpleTestCase extends Test {
    var field : String;

    public function new(){
        super();
    }

    public function setup(){
        field = "A";
    }

    public function testBasic():Void {
        Assert.equals(field, "A");
    }
}


class Test {
    public function new(){
    }

    static function main() {
        var r = new Runner();
        r.addCase(new SimpleTestCase());
        // add other TestCases here
        r.addCase(new HelloWorldTestCase());
        // finally, run the tests
        Report.create(r);
        r.run();
    }
}
