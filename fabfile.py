from fabric.api import *
from fabric.context_managers import lcd

NO_TARGET_MESSAGE = ('Please specify a target. For example "fab %s:js,php,neko".\n'
                     'Supported targets are js, php, python, neko, cs, java, cpp, hashlink, hashlink-c')

NO_TARGET_WIN_MESSAGE = ('Please specify a target. For example "fab %s:js,php,neko".\n'
                         'Supported targets are js, php, python, neko, cs, java, cpp')

def build_image(*args, progress='tty', **kwargs):
    if not args:
        print(NO_TARGET_MESSAGE % 'build_image')
        return
    for arg in args:
        local('DOCKER_BUILDKIT=1 docker build --progress=%s '
              '--target %s -t as204/haxe:%s-dev .' % (progress, arg, arg))

def make_target(*args, **kwargs):
    if not args:
        print(NO_TARGET_MESSAGE % 'make_target')
        return
    with lcd('test'):
        for arg in args:
            local('docker run --rm -v "$(pwd)":/tmp/hx -w /tmp/hx as204/haxe:%s-dev '
                  'haxe builds/%s.hxml' % (arg, arg))

def test_target(*args, **kwargs):

    if not args:
        print(NO_TARGET_MESSAGE % 'test_target')
        return
    with lcd('test'):
        for arg in args:
            local('docker run --rm -v "$(pwd)":/tmp/hx -w /tmp/hx as204/haxe:%s-dev '
                  'sh run-tests/%s.sh' % (arg, arg))

def run_testgame():
    local('docker run --rm -v "$(pwd)/FlixelTut":/tmp/hx --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" --device /dev/snd --privileged -w /tmp/hx as204/haxe:testgame-dev lime test linux --debug')


env.hosts=["vagrant@127.0.0.1:2222"]
env.passwords={"vagrant@127.0.0.1:2222": "vagrant"}
def win_configure():
    run("C:/tools/cygwin/bin/bash -c \"haxelib setup C:/Users/vagrant/haxelib\"", shell=False, pty=False)
    run("C:/tools/cygwin/bin/bash -c \"haxelib install hxcpp\"", shell=False, pty=False)
    run("C:/tools/cygwin/bin/bash -c \"haxelib install hxjava\"", shell=False, pty=False)
    run("C:/tools/cygwin/bin/bash -c \"haxelib install hxcs\"", shell=False, pty=False)
    run("C:/tools/cygwin/bin/bash -c \"haxelib install utest\"", shell=False, pty=False)

def win_make_target(*args, **kwargs):
    if not args:
        print(NO_TARGET_WIN_MESSAGE % 'win_make_target')
    for arg in args:
        run("C:/tools/cygwin/bin/bash -c \"cd C:/vagrant/test; haxe builds/win_%s.hxml\"" % arg,
            shell=False, pty=False)

def win_test_target(*args, **kwargs):
    if not args:
        print(NO_TARGET_WIN_MESSAGE % 'win_test_target')
    for arg in args:
        if arg == 'php':
            run("powershell -Command \"php C:/vagrant/test/build/win_php/index.php; Write-Host \"`> PHP passed\"\"",
                shell=False, pty=False)
        else:
            run("C:/tools/cygwin/bin/bash -c \"cd C:/vagrant/test; run-tests/win_%s.sh\"" % arg,
                shell=False, pty=False)
