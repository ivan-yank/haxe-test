FROM phusion/baseimage as base
MAINTAINER Ivan Yankovyi

ENV DEBIAN_FRONTEND noninteractive
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Dependencies
RUN apt-get update && apt-get install -y wget

# Haxe environment variables
ENV HAXEURL https://github.com/HaxeFoundation/haxe/releases/download/4.0.0-rc.1/haxe-4.0.0-rc.1-linux64.tar.gz
# ENV HAXEURL http://haxe.org/website-content/downloads/3.4.7/downloads/haxe-3.4.7-linux64.tar.gz
ENV HAXEPATH /root/haxe
ENV HAXE_STD_PATH $HAXEPATH/std/
ENV PATH $HAXEPATH:$PATH

RUN mkdir $HAXEPATH

# Download Haxe
RUN wget -O - $HAXEURL | tar xzf - --strip=1 -C $HAXEPATH

# Haxelib setup
RUN mkdir /root/haxelib
RUN echo /root/haxelib > /root/.haxelib
RUN cp /root/.haxelib /etc/

# Scripts
ADD scripts /root/scripts

ADD test /root/test
WORKDIR /root/test
RUN mkdir -p build

FROM base as neko_base
ENV NEKOURL http://nekovm.org/_media/neko-2.1.0-linux64.tar.gz
ENV NEKOPATH /root/neko
ENV LD_LIBRARY_PATH $NEKOPATH
ENV PATH $NEKOPATH:$PATH
RUN mkdir $NEKOPATH
# Download Neko
RUN wget -O - $NEKOURL | tar xzf - --strip=1 -C $NEKOPATH
RUN haxelib install utest

FROM neko_base as js
ENV NODEURL https://nodejs.org/dist/v8.6.0/node-v8.6.0-linux-x64.tar.gz
ENV NODEPATH /root/node
ENV PATH $NODEPATH/bin:$PATH
RUN mkdir $NODEPATH
RUN wget -O - $NODEURL | tar xzf - --strip=1 -C $NODEPATH

FROM neko_base as php
RUN apt-get install -y php7.0-cli php-mbstring

FROM neko_base as python

FROM neko_base as hashlink
RUN apt-get update && apt-get install -y --no-install-recommends \
  cmake \
  make \
  gcc \
  libz-dev \
  zlib1g-dev \
  libpng-dev \
  libsdl2-dev \
  libvorbis-dev \
  libalut-dev \
  libmbedtls-dev \
  libturbojpeg \
  libpng-dev \
  libvorbis-dev \
  libjpeg-turbo8-dev \
  libuv1-dev \
  libopenal-dev \
  neko \
  curl \
  ca-certificates \
  && rm -rf /var/lib/apt/lists/*
RUN ln -s /usr/lib/x86_64-linux-gnu/libturbojpeg.so.0 /usr/lib/x86_64-linux-gnu/libturbojpeg.so
RUN haxelib install hashlink
ENV HASHLINKURL https://github.com/HaxeFoundation/hashlink/archive/1.9.tar.gz
ENV HASHLINKPATH /root/hashlink
ENV PATH $HASHLINKPATH:$PATH
RUN mkdir $HASHLINKPATH
RUN wget -O - $HASHLINKURL | tar xzf - --strip=1 -C $HASHLINKPATH
WORKDIR $HASHLINKPATH
RUN ls
RUN set -ex && make all && make install && ldconfig
ENV ARCH=64

FROM hashlink as hashlink-c

FROM neko_base as cs
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb http://download.mono-project.com/repo/debian wheezy main" > /etc/apt/sources.list.d/mono-xamarin.list
RUN apt-get update
RUN apt-get install -y mono-devel
RUN haxelib install hxcs

FROM neko_base as java
RUN apt-get install -y default-jdk
RUN haxelib install hxjava

FROM neko_base as cpp
RUN apt-get install -y g++ g++-multilib libgc-dev
RUN haxelib install hxcpp
RUN haxelib install utest

FROM neko_base as neko

FROM neko_base as testgame
RUN apt-get update -yqq && apt-get install -yq \
  libgl1-mesa-dev \
  libglu1-mesa-dev \
  g++ \
  g++-multilib \
  gcc-multilib \
  libasound2-dev \
  libx11-dev \
  libxext-dev \
  libxi-dev \
  libxrandr-dev \
  libxinerama-dev \
  libgc1c2 \
  x11-apps
ENV TESTGAMEPATH /root/testgame
RUN mkdir $TESTGAMEPATH
ADD FlixelTut $TESTGAMEPATH
WORKDIR $TESTGAMEPATH
RUN haxelib newrepo $TESTGAMEPATH/.haxelib
RUN haxelib setup $TESTGAMEPATH/.haxelib
RUN cp $TESTGAMEPATH/.haxelib/lime/7,2,1/templates/bin/lime.sh /usr/local/bin/lime
RUN chmod 755 /usr/local/bin/lime
RUN echo $PATH
