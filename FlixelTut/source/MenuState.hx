package;

import flixel.FlxState;
import flixel.FlxG;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flash.system.System;

class MenuState extends FlxState
{
    var _btnPlay:FlxButton;
    var _txtTitle:FlxText;
    var _btnOptions:FlxButton;

    #if desktop
    var _btnExit:FlxButton;
    #end

	override public function create():Void
	{
        _txtTitle = new FlxText(20, 0, 0, "HaxeFlixelGame", 22);
        _txtTitle.alignment = CENTER;
        _txtTitle.screenCenter(X);
        add(_txtTitle);

        _btnPlay = new FlxButton(0, 0, "Play", clickPlay);
        _btnPlay.x = (FlxG.width / 2) - _btnPlay.width - 10;
        _btnPlay.y = FlxG.height - _btnPlay.height - 10;
        add(_btnPlay);
        _btnPlay.onUp.sound = FlxG.sound.load(AssetPaths.select__wav);

        _btnOptions = new FlxButton(0, 0, "Options", clickOptions);
        _btnOptions.x = (FlxG.width / 2) + 10;
        _btnOptions.y = FlxG.height - _btnOptions.height - 10;
        add(_btnOptions);

        #if desktop
        _btnExit = new FlxButton(FlxG.width - 28, 8, "X", clickExit);
        _btnExit.loadGraphic(AssetPaths.button__png, true, 20, 20);
        add(_btnExit);
        #end

        _btnOptions.onUp.sound = FlxG.sound.load(AssetPaths.select__wav);
        if (FlxG.sound.music == null)
        {
            #if flash
            FlxG.sound.playMusic(AssetPaths.HaxeFlixel_Tutorial_Game__mp3, 1, true);
            #else
            FlxG.sound.playMusic(AssetPaths.HaxeFlixel_Tutorial_Game__ogg, 1, true);
            #end
        }
		super.create();
	}

    #if desktop
    function clickExit():Void
    {
        System.exit(0);
    }
    #end

    function clickOptions():Void
    {
        FlxG.switchState(new OptionsState());
    }

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}

    function clickPlay():Void
    {
        FlxG.switchState(new PlayState());
    }
}
